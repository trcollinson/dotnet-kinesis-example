﻿using static Newtonsoft.Json.JsonConvert;
using Amazon.KinesisFirehose;
using Amazon.KinesisFirehose.Model;
using System;
using System.Threading.Tasks;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace Library
{
    public class LongJsonExample
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public DateTime CurrentTime  { get; set; }
        public DateTime CreateTime  { get; set; }
        public DateTime UpdateTime  { get; set; }
        public DateTime DeleteTime  { get; set; }
        public int ImportantNumber  { get; set; }
        public int LessImportantNumber  { get; set; }
        public string[] Things { get; set; }
        public string LoremIpsum { get; set; }
    }

    public class KinesisTest
    {
        public string GetExampleJson()
        {
            Random rnd = new Random();
            LongJsonExample example = new LongJsonExample();
            example.Name = "First " + rnd.Next(100);
            example.Type = "Object Type " + rnd.Next(1000);
            example.CurrentTime = DateTime.Now;
            example.CreateTime = DateTime.Now.AddHours(1);
            example.UpdateTime = DateTime.Now.AddDays(1);
            example.DeleteTime = DateTime.Now.AddMinutes(1);
            example.ImportantNumber = rnd.Next(42);
            example.LessImportantNumber = rnd.Next(21);
            string[] stuff = {"thing " + rnd.Next(10), "thing " + rnd.Next(10), "thing " + rnd.Next(10)};
            example.Things = stuff;
            example.LoremIpsum = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Curabitur pretium tincidunt lacus. Nulla gravida orci a odio. Nullam varius, turpis et commodo pharetra, est eros bibendum elit, nec luctus magna felis sollicitudin mauris. Integer in mauris eu nibh euismod gravida. Duis ac tellus et risus vulputate vehicula. Donec lobortis risus a elit. Etiam tempor. Ut ullamcorper, ligula eu tempor congue, eros est euismod turpis, id tincidunt sapien risus a quam. Maecenas fermentum consequat mi. Donec fermentum. Pellentesque malesuada nulla a mi. Duis sapien sem, aliquet nec, commodo eget, consequat quis, neque. Aliquam faucibus, elit ut dictum aliquet, felis nisl adipiscing sapien, sed malesuada diam lacus eget erat. Cras mollis scelerisque nunc. Nullam arcu. Aliquam consequat. Curabitur augue lorem, dapibus quis, laoreet et, pretium ac, nisi. Aenean magna nisl, mollis quis, molestie eu, feugiat in, orci. In hac habitasse platea dictumst.".Substring(rnd.Next(1, 500), rnd.Next(1, 200));
            return SerializeObject(example);
        }
        public async Task PutToFirehose()
        {
            String deliveryStreamName = "dotnet-demo-firehose-2";
            var client = new AmazonKinesisFirehoseClient();

            for (int i = 0; i < 20; i++)
            {
                var recordList = new List<Record>();
                
                for (int j = 0; j < 500; j++)
                {
                    Record record = new Record();
                    MemoryStream ms = new MemoryStream();
                    byte[] oByte = Encoding.UTF8.GetBytes(GetExampleJson());
                    ms.Write(oByte, 0, oByte.Length);
                    ms.WriteByte(0x0D);
                    record.Data = ms;
                    recordList.Add(record);
                }
                try{
                    PutRecordBatchResponse response = await client.PutRecordBatchAsync(deliveryStreamName, recordList);
                }
                catch (Exception e)
                {
                    Console.WriteLine(
                        "Unknown encountered on server. Message:'{0}' when writing an object"
                        , e.Message);
                }
            }
        }
    }
}