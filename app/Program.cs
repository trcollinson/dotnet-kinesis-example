﻿using System;
using static System.Console;
using Library;

namespace app
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine(new KinesisTest().GetExampleJson());
            new KinesisTest().PutToFirehose().Wait();
        }
    }
}
