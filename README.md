.Net AWS SDK Kinesis Firehose Example
=====================================

In this project we simply connect to a Kinesis Firehose and send data to the firehose in json format.

For more information on the presentation that is associated with this project, you can view the slides on [SlideShare](https://bit.ly/dotnetug-slides).


Athena Table Without Glue
-------------------------

It is possible to create an Athena queryable table without using Glue Crawlers and Glue Databases. For example, with this data you might do something like the following:

```
CREATE EXTERNAL TABLE IF NOT EXISTS demo_database.table_demo_1 (
  `Name` string,
  `Type` string,
  `CurrentTime` timestamp,
  `CreateTime` timestamp,
  `UpdateTime` timestamp,
  `DeleteTime` timestamp,
  `ImportantNumber` int,
  `LessImportantNumber` int,
  `Things` array<string>,
  `LoremIpsum` string 
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
  'serialization.format' = '1'
) LOCATION 's3://firehose-demo-test-bucket-1/2020/05/27/21/'
TBLPROPERTIES ('has_encrypted_data'='false');
```

This is because Athena stores the schema in a data catalog and uses it when you run a query. Athena uses schema-on-read, which means that the schema is projected on your data at the time you execute the query. Creating a table like this does not change your data in the Amazon S3 location.

That being said, it is a best practice to use Glue to create your database and table definitions. You do not need to run a crawler if you know your data structure and the Glue interface for creating a database, manipulating your data, and creating tables is much more robust.